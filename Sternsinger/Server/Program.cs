﻿using System;
using System.Text.Json;
using CC_Functions.AspNet;
using CC_Functions.Commandline;
using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.OpenApi.Models;
using OfficeOpenXml;
using static Sternsinger.Server.Database.Databases;

ArgsParse param = new(args);
ExcelPackage.LicenseContext = LicenseContext.NonCommercial;
bool printdb = param.GetBool("printdb");
bool hash = param["hash"] != null;
bool open = param.GetBool("open") || (!printdb && !hash);
if (hash) Console.WriteLine(Hash(param["hash"]!));
if (printdb)
    Console.WriteLine(JsonSerializer.Serialize(Registrations,
        new JsonSerializerOptions().AddCcf()));
if (open)
{
    string? pass;
#if DEBUG
    if ((pass = param.GetString("password-admin")) != null)
        PasswordAdminHash = Hash(pass);
    else if ((pass = Environment.GetEnvironmentVariable("PASSWORD_ADMIN")) != null)
        PasswordAdminHash = Hash(pass);
#else
    if (Environment.GetEnvironmentVariable("PASSWORD_ADMIN") != null || param.GetString("password-admin") != null)
        throw new ArgumentException("Explicit passwords are disabled in production");
#endif
    else if ((pass = param.GetString("password-admin-hash")) != null)
        PasswordAdminHash = pass;
    else if ((pass = Environment.GetEnvironmentVariable("PASSWORD_ADMIN_HASH")) != null)
        PasswordAdminHash = pass;
    else throw new ArgumentException("No password-admin provided, quitting");
    

    WebApplicationBuilder builder = WebApplication.CreateBuilder(args);
    builder.Services.AddCors();
    builder.Services.AddSwaggerGen(o => o.SwaggerDoc("v1", new OpenApiInfo
    {
        Title = "SternsingerAPI",
        Version = "v1"
    }));
    builder.Services.AddControllersWithViews().AddJsonOptions(options => options.JsonSerializerOptions.AddCcf());
    builder.Services.AddRazorPages();

    WebApplication app = builder.Build();
    if (app.Environment.IsDevelopment())
    {
        app.UseDeveloperExceptionPage();
        app.UseWebAssemblyDebugging();
    }
    else app.UseExceptionHandler("/Error");
#if FORCE_SSL
    app.UseHttpsRedirection();
#endif
    app.UseBlazorFrameworkFiles();
    app.UseStaticFiles();

    app.UseRouting();
    app.UseCors(x => x
        .AllowAnyMethod()
        .AllowAnyHeader()
        .SetIsOriginAllowed(_ => true)
        .AllowCredentials());

    app.UseSwagger(c => { c.RouteTemplate = "sternsinger/swagger/{documentName}/swagger.json"; });
    app.UseSwaggerUI(c =>
    {
        c.DocumentTitle = "Sternsinger API Swagger";
        c.RoutePrefix = "sternsinger/swagger";
        c.SwaggerEndpoint("/sternsinger/swagger/v1/swagger.json", "API");
    });

    app.MapRazorPages();
    app.MapControllers();
    app.MapFallbackToFile("index.html");
    app.Run();
}