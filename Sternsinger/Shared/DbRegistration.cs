using System;
using System.Collections.Generic;
using ProtoBuf;

namespace Sternsinger.Shared;

[ProtoContract]
public class DbRegistration
{
    [ProtoMember(1)] public string? Name { get; set; }
    [ProtoMember(2)] public string? House { get; set; }
    [ProtoMember(3)] public string? PhoneNumber { get; set; }
    [ProtoMember(4)] public Region? Region { get; set; }
    [ProtoMember(8)] public List<SelectedTime>? SelectedDays { get; set; }
    [ProtoMember(6)] public DateTime SignInTime { get; set; }
}