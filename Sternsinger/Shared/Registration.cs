﻿using System.Collections.Generic;

namespace Sternsinger.Shared;

public class Registration
{
    public string? Name { get; set; }
    public string? House { get; set; }
    public string? PhoneNumber { get; set; }
    public Region? Region { get; set; }
    public List<SelectedTime>? SelectedDays { get; set; }
}