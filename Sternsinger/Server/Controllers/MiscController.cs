﻿using System.Drawing;
using System.Globalization;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using OfficeOpenXml;
using OfficeOpenXml.Style;
using Sternsinger.Shared;
using static Sternsinger.Server.Database.Databases;
using Region = Sternsinger.Shared.Region;

namespace Sternsinger.Server.Controllers;

[ApiController]
[Route("[controller]")]
public class MiscController : ControllerBase
{
    [HttpGet("export.xlsx", Name = nameof(ExportData))]
    [ProducesResponseType(200)]
    [ProducesResponseType(400, Type = typeof(JfException))]
    public ActionResult ExportData(string password)
    {
        if (Hash(password) == PasswordAdminHash)
        {
            using ExcelPackage package = new();
            foreach (IGrouping<Region, DbRegistration> registrations in Registrations.Select(s => s.Value).GroupBy(s => s.Region!.Value))
            {
                ExcelWorksheet ws = package.Workbook.Worksheets.Add(registrations.Key.GetDisplayString());
                int row = 1;
                int column = 0;
                FormatCell(ws, row, ++column, "Urzeit der Eintragung", true, true, true);
                FormatCell(ws, row, ++column, "Haus", true, true, true);
                FormatCell(ws, row, ++column, "Name", true, true, true);
                FormatCell(ws, row, ++column, "Nummer", true, true, true);
                FormatCell(ws, row, ++column, "Gewählte Tage", true, true, true);
                foreach (DbRegistration registration in registrations)
                {
                    column = 0;
                    row++;
                    FormatCell(ws, row, ++column, registration.SignInTime.ToString(CultureInfo.InvariantCulture), table: true);
                    FormatCell(ws, row, ++column, registration.House!, table: true);
                    FormatCell(ws, row, ++column, registration.Name!, table: true);
                    FormatCell(ws, row, ++column, registration.PhoneNumber!, table: true);
                    FormatCell(ws, row, ++column, registration.SelectedDays!.GetDisplayString(registrations.Key), table: true);
                }
                for (int i = 1; i <= column; i++)
                    ws.Column(i).AutoFit();
            }
            if (package.Workbook.Worksheets.Count == 0)
                FormatCell(package.Workbook.Worksheets.Add("Keine Einträge"), 1, 1, "Noch keine Einträge");
            return new FileContentResult(package.GetAsByteArray(), "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
        } else return Unauthorized(new JfException("Unauthorisiert", "Nur Administratoren haben hierauf Zugriff"));
    }

    [HttpGet("checkpwd", Name = nameof(CheckPassword))]
    public ActionResult<bool> CheckPassword(string password) => Hash(password) == PasswordAdminHash;

    private void FormatCell(ExcelWorksheet ws, int row, int column, string? text = null, bool bold = false, bool table = false, bool header = false)
    {
        if (text != null)
        {
            ws.Cells[row, column].Value = text;
            ws.Cells[row, column].Style.Font.Name = "Helvetica";
        }
        if (bold) ws.Cells[row, column].Style.Font.Bold = true;
        if (table) ws.Cells[row, column].Style.Border.BorderAround(ExcelBorderStyle.Thin);
        if (header) ws.Cells[row, column].Style.Fill.SetBackground(Color.Khaki);
    }
}