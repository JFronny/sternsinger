﻿namespace Sternsinger.Shared;

public enum Region
{
    WeilDerStadt,
    Merklingen,
    Schafhausen,
    Ostelsheim,
    Münklingen,
    Hausen
}

public static class RegionFunc
{
    public static bool IsValid(this Region? d) =>
        d == Region.WeilDerStadt || d == Region.Merklingen || d == Region.Schafhausen ||
        d == Region.Ostelsheim || d == Region.Münklingen || d == Region.Hausen;

    public static string GetDisplayString(this Region d) =>
        d switch
        {
            Region.WeilDerStadt => "Weil der Stadt",
            _ => d.ToString()
        };

    public static bool IsSecondary(this Region d) =>
        d == Region.Merklingen || d == Region.Münklingen || d == Region.Hausen;
}