﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;

namespace Sternsinger.Shared;

public enum Day
{
    Day1,
    Day2,
    Day3,
    Day4
}

public enum Timeframe
{
    Early,
    Mid,
    Late
}

public static class DaysFunc
{
    public static DateTime GetDate(this Day d, Region region) =>
        new(2026, 1, 2 + d switch
        {
            Day.Day1 => 1,
            Day.Day2 => 2,
            Day.Day3 => 3,
            Day.Day4 => 4,
            _ => throw new InvalidEnumArgumentException(nameof(d))
        });

    public static Tuple<DateTime, DateTime> GetTime(this Day d, Region region, Timeframe frame)
    {
        DateTime date = d.GetDate(region);
        // T1 start, T1 end = T2 start, T2 end = T3 start, T3 end
        Tuple<int, int, int, int> tup = region.IsSecondary()
            ? d switch
            {
                Day.Day1 => new Tuple<int, int, int, int>(16, 18, 20, -1),
                Day.Day2 => new Tuple<int, int, int, int>(14, 16, 18, 20),
                Day.Day3 => new Tuple<int, int, int, int>(14, 16, 18, 20),
                Day.Day4 => new Tuple<int, int, int, int>(14, 16, -1, -1),
                _ => throw new InvalidEnumArgumentException(nameof(d))
            }
            : d switch
            {
                Day.Day1 => new Tuple<int, int, int, int>(14, 16, 18, 19),
                Day.Day2 => new Tuple<int, int, int, int>(14, 16, 18, -1),
                Day.Day3 => new Tuple<int, int, int, int>(14, 16, 18, 19),
                Day.Day4 => new Tuple<int, int, int, int>(14, 16, -1, -1),
                _ => throw new InvalidEnumArgumentException(nameof(d))
            };
        return new Tuple<DateTime, DateTime>(date + new TimeSpan(frame switch
        {
            Timeframe.Early => tup.Item1,
            Timeframe.Mid => tup.Item2,
            Timeframe.Late => tup.Item3,
            _ => throw new InvalidEnumArgumentException(nameof(frame))
        }, 0, 0), date + new TimeSpan(frame switch
        {
            Timeframe.Early => tup.Item2,
            Timeframe.Mid => tup.Item3,
            Timeframe.Late => tup.Item4,
            _ => throw new InvalidEnumArgumentException(nameof(frame))
        }, 0, 0));
    }

    public static bool IsValid(this Timeframe frame, Day day, Region region)
    {
        if (Enum.GetValues<Timeframe>().All(t => t != frame)) return false;
        if (Enum.GetValues<Day>().All(t => t != day)) return false;
        return frame switch
        {
            Timeframe.Late when region.IsSecondary() => day == Day.Day2 || day == Day.Day3,
            Timeframe.Late when !region.IsSecondary() => day == Day.Day1 || day == Day.Day3,
            Timeframe.Mid => day != Day.Day4,
            _ => true
        };
    }

    public static string GetDisplayString(this IEnumerable<SelectedTime> d, Region region) => string.Join(", ", d.Select(s => s.Day.GetDisplayString(s.Timeframe, region)));

    public static string GetDisplayString(this Day day, Timeframe timeframe, Region region)
    {
        Tuple<DateTime, DateTime> frame = day.GetTime(region, timeframe);
        return day.GetDate(region).ToShortDateString() + " - " +
               frame.Item1.ToShortTimeString() + " bis " + frame.Item2.ToShortTimeString();
    }
}