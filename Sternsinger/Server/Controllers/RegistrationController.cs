﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using Sternsinger.Shared;
using static Sternsinger.Server.Database.Databases;

namespace Sternsinger.Server.Controllers;

[ApiController]
[Route("[controller]")]
public class RegistrationController : ControllerBase
{
    private static readonly string[] InvalidNumbers =
    [
        "1234",
        "12345",
        "4321",
        "54321",
        "123",
        "2222"
    ];

    [HttpPut(Name = nameof(Register))]
    [ProducesResponseType(200, Type = typeof(Guid))]
    [ProducesResponseType(400, Type = typeof(JfException))]
    public ActionResult<Guid> Register(Registration registration)
    {
        if (registration.SelectedDays == null || registration.SelectedDays.Count == 0)
            return BadRequest(new JfException("Kein Tag angegeben", "Sie haben keinen Tag angegeben"));
        if (string.IsNullOrWhiteSpace(registration.Name) || string.IsNullOrWhiteSpace(registration.House) ||
            string.IsNullOrWhiteSpace(registration.PhoneNumber))
            return BadRequest(new JfException("Daten unvollständig", "Die angegebenen Daten sind unvollständig"));
        if (registration.Name.Split(",").Length != 2)
            return BadRequest(new JfException("Falsche Eingabe", "Der Name ist falsch angegeben"));
        if (registration.Name.Split(",")[0].Trim().Length == 0)
            return BadRequest(new JfException("Falsche Eingabe", "Kein Nachname angegeben"));
        if (registration.Name.Split(",")[1].Trim().Length == 0)
            return BadRequest(new JfException("Falsche Eingabe", "Kein Vorname angegeben"));
        if (registration.Name.Length > 100)
            return BadRequest(new JfException("Name zu lang", "Der angegebene Name ist zu lang"));
        if (!registration.Region.IsValid())
            return BadRequest(new JfException("Inkorrekte Regionsangabe", "Keine valide Region angegeben"));
        if (!registration.SelectedDays.TrueForAll(day => day.Timeframe.IsValid(day.Day, registration.Region!.Value)))
            return BadRequest(new JfException("Unmöglicher Tag",
                "Zu dieser Zeit kommen die Sternsinger an diesem Tag nicht nach " +
                registration.Region!.Value.GetDisplayString()));
        if (registration.PhoneNumber.Length > 15 || InvalidNumbers.Contains(registration.PhoneNumber))
            return BadRequest(new JfException("Inkorrekte Telefonnummer",
                "Bitte geben sie eine korrekte Telefonnummer an"));
        if (Registrations.Select(s => s.Value).Any(s =>
                s.House == registration.House || s.Name == registration.Name ||
                s.PhoneNumber == registration.PhoneNumber))
            return BadRequest(new JfException("Bereits registriert", "Sie sind bereits registriert"));
        Guid p = Guid.NewGuid();
        DbRegistration t = new()
        {
            House = registration.House,
            Name = registration.Name,
            PhoneNumber = registration.PhoneNumber,
            Region = registration.Region,
            SelectedDays = registration.SelectedDays,
            SignInTime = DateTime.Now
        };
        Registrations.Add(p, t);
        return Ok(p);
    }

    [HttpGet(Name = nameof(GetRegistrations))]
    [ProducesResponseType(200, Type = typeof(IDictionary<Guid, DbRegistration>))]
    [ProducesResponseType(401, Type = typeof(JfException))]
    public ActionResult<IDictionary<Guid, DbRegistration>> GetRegistrations(string password)
    {
        if (Hash(password) == PasswordAdminHash) return Registrations;
        return Unauthorized(new JfException("Unauthorisiert", "Nur Administratoren haben hierauf Zugriff"));
    }

    [HttpDelete(Name = nameof(RemoveRegistration))]
    [ProducesResponseType(200)]
    [ProducesResponseType(400, Type = typeof(JfException))]
    [ProducesResponseType(401, Type = typeof(JfException))]
    [ProducesResponseType(404, Type = typeof(JfException))]
    public ActionResult RemoveRegistration(string password, Guid registration)
    {
        if (Hash(password) == PasswordAdminHash)
        {
            if (Registrations.Remove(registration)) return Ok();
            return NotFound(new JfException("Nicht gefunden", "Die angegebene Registrierung existiert nicht"));
        }
        return Unauthorized(new JfException("Unauthorisiert", "Nur Administratoren haben hierauf Zugriff"));
    }
}