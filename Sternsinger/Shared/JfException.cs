﻿namespace Sternsinger.Shared;

public class JfException
{
    public JfException(string message, string details)
    {
        Message = message;
        Details = details;
    }

    public string Message { get; set; }
    public string Details { get; set; }
}