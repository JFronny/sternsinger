﻿using System;
using CC_Functions.AspNet;
using Sternsinger.Shared;

namespace Sternsinger.Server.Database;

public class RegistrationD : SerialDict<DbRegistration>
{
    public RegistrationD()
    {
        Loaded += dictionary =>
        {
            foreach (DbRegistration value in dictionary.Values)
            {
                value.House ??= "";
                value.Name ??= "";
                value.PhoneNumber ??= "";
                value.SignInTime = value.SignInTime == default ? new DateTime(2026, 1, 1) : value.SignInTime;
            }
        };
    }

    public override string DatabasesDir => Databases.Dir;
    public override string DatabaseFileName => $"registrations.{Databases.Extension}";
}