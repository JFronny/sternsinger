FROM mcr.microsoft.com/dotnet/sdk:8.0 AS build-env
WORKDIR /app

COPY . ./

RUN dotnet publish Sternsinger/Server/Sternsinger.Server.csproj -c Release -o out
RUN rm out/Sternsinger.Server

FROM mcr.microsoft.com/dotnet/aspnet:8.0-bookworm-slim-arm64v8
WORKDIR /app
COPY --from=build-env /app/out .
ENTRYPOINT ["dotnet", "Sternsinger.Server.dll"]
