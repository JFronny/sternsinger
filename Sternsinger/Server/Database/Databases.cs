﻿using System.IO;
using System.Reflection;
using System.Security.Cryptography;
using System.Text;

namespace Sternsinger.Server.Database;

public static class Databases
{
    public const string Extension = "db";

    public static readonly string Dir =
        Path.Combine(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location)!, "DB");

    public static readonly RegistrationD Registrations = new();

    public static string PasswordAdminHash = Hash("admin"); // set manually from entrypoint to not be this

    private static MD5 _md5 = MD5.Create();

    public static string Hash(string value)
    {
        _md5 ??= MD5.Create(); // this is null otherwise. I don't know why, nor do I care.
        byte[] pwdBytes = _md5.ComputeHash(Encoding.Unicode.GetBytes(value));
        StringBuilder sb = new();
        foreach (byte t in pwdBytes)
            sb.Append(t.ToString("X2"));
        return sb.ToString();
    }
}