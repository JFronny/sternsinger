using ProtoBuf;

namespace Sternsinger.Shared;

[ProtoContract]
public class SelectedTime
{
    [ProtoMember(1)] public Day Day { get; set; }
    [ProtoMember(2)] public Timeframe Timeframe { get; set; }

    public override bool Equals(object? obj)
    {
        if (obj == null || GetType() != obj.GetType()) return false;
        return GetHashCode().Equals(obj.GetHashCode());
    }

    public override int GetHashCode() => (Day.GetHashCode() * 4) ^ Timeframe.GetHashCode();

    public override string ToString() => Day + "-" + Timeframe;
}